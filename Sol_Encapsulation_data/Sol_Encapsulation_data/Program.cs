﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Encapsulation_data
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentEntity studentEntityObj = new StudentEntity()
            {
                FirstName = "presty",
                LastName = "prajna"
            };

            Test testObj = new Test(studentEntityObj);
            testObj.Display();
           

            StudentEntity studentEntityObj1 = new StudentEntity()
            {
                FirstName = "deepak",
                LastName = "kotian"
            };

            Test testObj1 = new Test(studentEntityObj1);
            testObj1.Display();


        }
    }

    public class StudentEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }

    public class Test
    {
        private StudentEntity studentEntityObj = null;

        public Test(StudentEntity studentEntityObj)
        {
            this.studentEntityObj = studentEntityObj;
        }

        public void Display()
        {
            Console.WriteLine(studentEntityObj.FirstName);
            Console.WriteLine(studentEntityObj.LastName);
        }
    }
}
